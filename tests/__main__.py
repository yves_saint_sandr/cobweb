import unittest

from tests.units.test_http_adapter import TestHttpAdapter
from tests.units.test_settings import TestSettings
from tests.units.test_bloom_filter import TestBloomFilter
from tests.units.test_link_extractor import TestLinkExtractor
from tests.units.test_spider import TestSpider
from tests.units.test_image_adapter import TestImageAdapter


if __name__ == "__main__":

    tests = [
        TestHttpAdapter,
        TestSettings,
        TestBloomFilter,
        TestLinkExtractor,
        TestSpider,
        TestImageAdapter,
    ]

    suite = unittest.TestSuite([test.prepare_suite() for test in tests])
    unittest.TextTestRunner(verbosity=2).run(suite)
