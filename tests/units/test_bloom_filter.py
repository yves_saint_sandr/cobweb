from tests.units.abstract_test import AbstractTest
from cobweb.link_extractor.bloom_filter import BloomFilter


class TestBloomFilter(AbstractTest):

    def setUp(self):
        self.bloom_filter = BloomFilter()

    def test_empty_set(self):
        test_for = "should't detect string presence in empty set"
        self.assertFalse("this is a test string" in self.bloom_filter, test_for)
        self.assertFalse("look, here is another test string" in self.bloom_filter, test_for)

    def test_contains_method(self):
        self.bloom_filter.add("item one")
        self.bloom_filter.add("item two")
        self.assertFalse("item three" in self.bloom_filter, "shouldn't detect strings that aren't in filter")
        self.assertTrue("item two" in self.bloom_filter, "should detect strings that are in filter")
        self.assertAlmostEqual(self.bloom_filter.count_error_probability(), 2.27e-05, msg="should count loss")
