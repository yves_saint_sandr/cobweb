import os
import unittest
import urllib3

from tests.units.abstract_test import AbstractTest
from cobweb.adapters.image import ImageAdapter
from cobweb.helpers import is_connected_to_internet


class TestImageAdapter(AbstractTest):

    @unittest.skipUnless(is_connected_to_internet(), "no internet connection")
    def test_image_save(self):
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        adapter = ImageAdapter()
        url = "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Kinkaku-ji_in_November_2016_-02.jpg/800px-Kinkaku-ji_in_November_2016_-02.jpg"
        adapter.invoke(url).data.save("image.jpg")

    def tearDown(self):
        os.remove("image.jpg")
