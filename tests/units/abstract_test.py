import unittest

from abc import ABCMeta


class AbstractTest(unittest.TestCase, metaclass=ABCMeta):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    @classmethod
    def prepare_suite(cls):
        return unittest.TestLoader().loadTestsFromTestCase(cls)
