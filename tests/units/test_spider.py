from tests.units.abstract_test import AbstractTest


from multiprocessing import Queue
from cobweb.spiders import Spider
from cobweb import Settings


class TestSpider(AbstractTest):

    def setUp(self):

        def dummy_func(*args, **kwargs):
            pass

        def raise_an_exception(*args, **kwargs):
            raise KeyboardInterrupt("Waited for too long")

        settings = Settings()
        settings.keep_alive_timeout = 0.1
        self.queue = Queue()
        self.spider = Spider(self.queue, settings=settings)
        self.dummy_func = dummy_func
        self.raise_exception = raise_an_exception

    def test_correct_function_setters(self):
        self.spider.on_failure_callback = self.dummy_func
        self.spider.on_failure_callback = None

        with self.assertRaises(ValueError):
            self.spider.on_failure_callback = "String is not suitable here"

    def test_failure_with_empty_process_callback(self):
        with self.assertRaises(ValueError):
            # start raises exception from other process and test fails
            self.spider.run()

    def test_successful_start_with_process_callback(self):
        self.spider.process_response = self.dummy_func
        self.spider.on_queue_timeout = self.raise_exception
        with self.assertRaises(KeyboardInterrupt):
            self.spider.run()
