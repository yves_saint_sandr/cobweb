import unittest
import urllib3

from tests.units.abstract_test import AbstractTest
from cobweb.adapters.http import HttpAdapter
from cobweb.adapters.dynamic_http import DynamicHttpAdapter
from cobweb.helpers import is_connected_to_internet


class TestHttpAdapter(AbstractTest):

    @unittest.skipUnless(is_connected_to_internet(), "no internet connection")
    def test_status_codes(self):
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        page = HttpAdapter()
        existing_page = "https://mrakopedia.org/"
        nonexistent_page = "https://github.com/hellomyworlds"
        self.assertEqual(page.invoke(existing_page).status_code, 200, "page exists")
        self.assertEqual(page.invoke(nonexistent_page).status_code, 404, "page doesn't exist")

    @unittest.skip("selenium no longer tested")
    def test_dynamic_adapter(self):
        existing_page = "https://mrakopedia.org/"

        adapter = DynamicHttpAdapter(webdriver=DynamicHttpAdapter.FirefoxDriver)
        adapter.invoke(existing_page)
