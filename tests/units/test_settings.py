import json
import yaml
import os

from tests.units.abstract_test import AbstractTest
from cobweb.settings import Settings


class TestSettings(AbstractTest):

    def setUp(self):

        json_filename = os.path.join(
            *os.path.split(os.getcwd()),
            "data",
            "sett.json"
        )
        yaml_filename = os.path.join(
            *os.path.split(os.getcwd()),
            "data",
            "sett.yml"
        )
        self.json_settings = Settings.from_json(json_filename)
        self.yaml_settings = Settings.from_yaml(yaml_filename)

        with open(json_filename) as f:
            self.json_data = json.load(f)

        with open(yaml_filename) as f:
            self.yaml_data = yaml.load(f)

    def test_correct_json_load(self):
        test_for = "should load settings from json"
        self.assertEqual(self.json_data["on_failure_action"], self.json_settings.on_failure_action, test_for)
        self.assertEqual(self.json_data["use_bbs"], self.json_settings.use_bbs, test_for)

    def test_correct_yaml_load(self):
        test_for = "should load settings from yaml"
        self.assertEqual(self.yaml_data["on_failure_action"], self.yaml_settings.on_failure_action, test_for)
        self.assertEqual(self.yaml_data["use_bbs"], self.yaml_settings.use_bbs, test_for)

    def test_to_set_conversion(self):
        test_for = "should convert status_codes to set"
        self.assertIsInstance(self.json_settings.success_codes, set, test_for)
        self.assertIsInstance(self.json_settings.failure_codes, set, test_for)
        self.assertIsInstance(self.yaml_settings.success_codes, set, test_for)
        self.assertIsInstance(self.yaml_settings.failure_codes, set, test_for)

    def test_whitelist(self):
        with self.assertRaises(AttributeError):
            self.json_settings.corrupted_attribute

        with self.assertRaises(AttributeError):
            self.yaml_settings.corrupted_attribute
