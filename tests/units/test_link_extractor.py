import unittest

from tests.units.abstract_test import AbstractTest

from cobweb.link_extractor import LinkExtractor
from cobweb.helpers import is_connected_to_internet
from cobweb.adapters.http import HttpAdapter


class TestLinkExtractor(AbstractTest):

    def setUp(self):
        self.adapter = HttpAdapter()

    def test_domain_only_check(self):
        link_extractor = LinkExtractor(domain_only=True)
        link_extractor.current_page_url = "http://www.dustyfeet.com/"

        test_for = "should accept links only with the same domain when 'domain_only' is on"
        self.assertTrue(
            link_extractor.is_valid_domain('http://www.dustyfeet.com/mykeyboardbaby1.html'),
            test_for
        )
        self.assertTrue(
            link_extractor.is_valid_domain('http://www.dustyfeet.com/pangan/index.html'),
            test_for
        )

        test_for = "shouldn't accept links with different domain when 'domain_only' is on"
        self.assertFalse(
            link_extractor.is_valid_domain('http://www.dustyhfeet.com/pangan/index.html'),
            test_for
        )
        self.assertFalse(
            link_extractor.is_valid_domain('https://wikipedia.com/Article'),
            test_for
        )

    def test_domain_only_skip(self):
        link_extractor = LinkExtractor()
        link_extractor.domain_only = False
        link_extractor.current_page_url = "http://www.dustyfeet.com/"

        test_for = "should accept all links with the same domain when 'domain_only' is off"
        self.assertTrue(
            link_extractor.is_valid_domain('http://www.dustyfeet.com/mykeyboardbaby1.html'),
            test_for
        )
        self.assertTrue(
            link_extractor.is_valid_domain('http://www.dustyfeet.com/pangan/index.html'),
            test_for
        )
        self.assertTrue(
            link_extractor.is_valid_domain('http://www.dustyhfeet.com/pangan/index.html'),
            test_for
        )
        self.assertTrue(
            link_extractor.is_valid_domain('https://wikipedia.com/Article'),
            test_for
        )

    def test_include_patterns(self):
        link_extractor = LinkExtractor()
        link_extractor.follow_patterns = ["*mrakopedia*"]
        link_extractor.exclude_patterns = "*.com"

        self.assertTrue(link_extractor.is_satisfying_link("http://mrakopedia.org/"))
        self.assertTrue(link_extractor.is_satisfying_link("http://pre.domain.mrakopedia.ru/"))

        self.assertFalse(link_extractor.is_satisfying_link("http://mrakopedia.com"))
        self.assertFalse(link_extractor.is_satisfying_link("http://www.wikipedia.com"))
